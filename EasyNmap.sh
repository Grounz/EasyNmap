#!/bin/bash
###########
#KLT#######
###########

show_menu(){
    NORMAL=`echo "\033[m"`
    MENU=`echo "\033[36m"` #Blue
    NUMBER=`echo "\033[33m"` #yellow
    FGRED=`echo "\033[41m"`
    RED_TEXT=`echo "\033[31m"`
    ENTER_LINE=`echo "\033[33m"`
    echo -e "${MENU}*********************************************${NORMAL}"
    echo -e "${MENU}**${NUMBER} 1)${MENU} Advanced Furtiv Scan ${NORMAL}"
    echo -e "${MENU}**${NUMBER} 2)${MENU} TCP Scan ${NORMAL}"
    echo -e "${MENU}**${NUMBER} 3)${MENU} UDP Scan ${NORMAL}"
    echo -e "${MENU}**${NUMBER} 4)${MENU} Evasion Scan ${NORMAL}"
    echo -e "${MENU}**${NUMBER} 5)${MENU} Exit ${NORMAL}"
    echo -e "${MENU}*********************************************${NORMAL}"
    echo -e "${ENTER_LINE}Please enter a menu option and enter or ${RED_TEXT}enter to exit. ${NORMAL}"
    read opt
}
perso_scan(){

x="1"
while [ "$x"  -ne "0" ]
do
    if [ "$opt" = "1" ]
        then
	    echo -e "${MENU}*********************************************${NORMAL}Menu Furtif ${MENU}*********************************************${NORMAL}: "
            echo -e "Choose your scan type : "
            echo -e "${MENU}**${NUMBER} 1)${MENU}Scan XMas (Flags FIN/PSH/URG) :${NORMAL}"	
            echo -e "${MENU}**${NUMBER} 2)${MENU}Scan Fin  (Flags FIN) : ${NORMAL}"
            echo -e "${MENU}**${NUMBER} 3)${MENU}Scan NULL (Header=0) :${NORMAL} "
            echo -e "${MENU}**${NUMBER} 4)${MENU}TCP Scan ACK (Detect Firewall ): ${NORMAL}"
            echo -e "${MENU}**${NUMBER} 5)${MENU}TCP Scan MAIMON (BSD TARGET / FLags FIN/ACK) : ${NORMAL}"
	    echo -e "${MENU}**${NUMBER} 6)${MENU}Exit : ${NORMAL}"
            echo -e "${ENTER_LINE}Please enter a menu option and enter or ${RED_TEXT}enter to exit. ${NORMAL}"
            read chx
		if [ $chx = "1" ]
			then 
			        sudo nmap -T0 -sX $target --reason
				exit 0
		elif [ $chx = "2" ]
			then
				sudo nmap -T0 -sF $target --reason
				exit 0
		elif [ $chx = "3" ]
			then 
				sudo nmap -T0 -sN $target --reason
				exit 0
		elif [ $chx = "4" ] 
			then
				sudo nmap -T0 -sA $target --reason
				exit 0
		elif [ $chx = "5" ]
			then
			   	sudo nmap -T0 -sM $target --reason
				exit 0
		elif [ $chx = "6" ]
			then
			   	exit 0
		else
		        exit 1
		fi

	elif [ "$opt" = "2" ]
	   then 
	       echo -e "${MENU}*********************************************${NORMAL}Menu TCP ${MENU}*********************************************${NORMAL}: "
               echo -e " Choose your scan type : "
               echo -e "${MENU}**${NUMBER} 1)${MENU}Insane Temporisation + TCP SYN + OS Detection + Version Detection :${NORMAL} "
               echo -e "${MENU}**${NUMBER} 2)${MENU}Agressive Temporistation + TCP SYN + OS Detection + Version Detection :${NORMAL}"
	       echo -e	"${MENU}**${NUMBER} 3)${MENU}Paranoid Temporisation + TCP SYN + OS Detection + Version Detection :${NORMAL}"
	       echo -e "${MENU}**${NUMBER} 4)${MENU}Insane Temporisation + TCP Connect + OS Detection  + Version Detection :${NORMAL}"
	       echo -e "${MENU}**${NUMBER} 5)${MENU}Agressive Temporisation + TCP Connect + OS Detection + Version Detection :${NORMAL}"
	       echo -e "${MENU}**${NUMBER} 6)${MENU}Paranoid Temporisation + TCP COnnect + OS Detection + Version Detection :${NORMAL}"
	       echo -e "${MENU}**${NUMBER} 7)${MENU}Exit :${NORMAL}"
               echo -e "${ENTER_LINE}Please enter a menu option and enter or ${RED_TEXT}enter to exit. ${NORMAL}"
               read chx
		if [ $chx = "1" ]
            		then
                		sudo nmap -T5 -sS -A $target --reason
                 		exit 0
        	elif [ $chx = "2" ]
            		then
                		sudo nmap -T4 -sS -A $target --reason
                		exit 0
		elif [ $chx = "3" ]
			then
				sudo nmap -T0 -sS -A $target --reason
		elif [ $chx = "4" ]
			then
				sudo nmap -T5 -sT -A $target --reason
				exit 0
		elif [ $chx = "5" ]
			then
				sudo nmap -T4 -sT -A $target --reason
				exit 0
		elif [ $chx = "6" ]
			then
				sudo nmap -T0 -sT -A $target --reason
				exit 0
		elif [ $chx = "7" ]
			then
				exit 0
        	else
                		echo "Bad Character !!!!"
		        	exit 1
		fi
	elif [ "$opt" = "3" ]
	   then
	       echo -e "${MENU}*********************************************${NORMAL}Menu UDP  ${MENU}*********************************************${NORMAL}: "
	       echo -e " Choose your scan type : "
               echo -e "${MENU}**${NUMBER} 1)${MENU}Insane Temporisation + UDP Scan + OS Detection + Version Detection :${NORMAL} "
               echo -e "${MENU}**${NUMBER} 2)${MENU}Agressive Temporistation + UDP Scan + OS Detection + Version Detection :${NORMAL}"
               echo -e "${MENU}**${NUMBER} 3)${MENU}Paranoid Temporisation + UDP Scan + OS Detection + Version Detection :${NORMAL}"
               echo -e "${MENU}**${NUMBER} 4)${MENU}Insane Temporisation + UDP Scan + OS Detection  + Version Detection :${NORMAL}"
               echo -e "${MENU}**${NUMBER} 5)${MENU}Agressive Temporisation + UDP Scan + OS Detection + Version Detection :${NORMAL}"
               echo -e "${MENU}**${NUMBER} 6)${MENU}Paranoid Temporisation + UDP Scan + OS Detection + Version Detection :${NORMAL}"
               echo -e "${MENU}**${NUMBER} 7)${MENU}Exit :${NORMAL}"
               echo -e "${ENTER_LINE}Please enter a menu option and enter or ${RED_TEXT}enter to exit. ${NORMAL}"
               read chx
        	if [ $chx = "1" ]
            		then
                		sudo nmap -T5 -sU -A $target --reason
                		exit 0
        	elif [ $chx = "2" ]
            		then
               	                sudo nmap -T4 -sU -A $target --reason
                		exit 0
        	elif [ $chx = "3" ]
            		then
                		sudo nmap -T0 -sU -A $target --reason
				exit 0
        	elif [ $chx = "4" ]
            		then
               			sudo nmap -T5 -sU -A $target --reason
                		exit 0
        	elif [ $chx = "5" ]
            		then
                		sudo nmap -T4 -sU -A $target --reason
                		exit 0
        	elif [ $chx = "6" ]
            		then
                		sudo nmap -T0 -sU -A $target --reason
                		exit 0
        	elif [ $chx = "7" ]
            		then
                		exit 0
        	else
        			echo "Bad Character !!!!"
		    		exit 1
        	fi      
	elif [ $opt = "4" ]
	   then
	       echo -e "${MENU}*********************************************${NORMAL}Menu Evasion  ${MENU}*********************************************${NORMAL}: "
	       echo -e " Choose your scan type : "
               echo -e "${MENU}**${NUMBER} 1)${MENU}No ping + Decoy Option:${NORMAL} "
               echo -e "${MENU}**${NUMBER} 2)${MENU}No Ping + Decoy Option + Idle Scan:${NORMAL}"
               echo -e "${MENU}**${NUMBER} 3)${MENU}No Ping + Decoy Option + Idle Scan + Spoof source port:${NORMAL}"
               echo -e "${MENU}**${NUMBER} 4)${MENU}No Ping + Decoy Option + Idle Scan + + Delay + Spoof source port:${NORMAL}"
               echo -e "${MENU}**${NUMBER} 5)${MENU}Scan over TOR + No Ping + TCP/UDP + OS and Version Services Detection:${NORMAL}"
               echo -e "${MENU}**${NUMBER} 6)${MENU}Exit :${NORMAL}"
	       read chx
		if [ $chx = "1" ]
            then
                echo -e "Enter ip address or Hostname for Decoy option, for several decoy parameters separate with comma : "
                read decoy
                sudo nmap -P0 -D $decoy $target --reason
                exit 0
        elif [ $chx = "2" ]
            then
                echo -e "Enter ip address or Hostname for Decoy option, for several decoy parameters separate with comma : "
                read decoy
                echo -e "Enter ip address for Idle scan ex 192.168.1.1:80 don't forget to give open port : "
                read idle
                sudo nmap -P0 -D $decoy -sI $idle $target --reason
                exit 0
        elif [ $chx = "3" ]
            then
                echo -e "Enter ip address or Hostname for Decoy option  ex : ipaddress1,ipaddress2,you-own-ip,ipaddress3 or fqdn: "
                read decoy
                echo -e "nter ip address for Idle scan ex 192.168.1.1:80 don't forget to enter open port : "
                read idle
                echo -e "Enter spoof source port :"
                read spoofport
                sudo nmap -P0 -g $spoofport -D $decoy -sI $idle $target --reason
                exit 0
        elif [ $chx = "4" ]
            then
                echo -e "Enter ip address or Hostname for Decoy option : "
                read decoy
                echo -e "Enter ip address for Idle scan ex 192.168.1.1:80 don't forget to enter open port : "
                read idle
                echo -e "Enter delay for scan :"
                read delay
                echo -e "Enter spoof source port :"
                read spoofport
                sudo nmap -P0 --scan-delay $delay -g $spoofport -D $decoy -sI $idle $target --reason
                exit 0
        elif [ $chx = "5" ]
            then
                echo -e "Scan over tor :"
                exit 0
        else
                exit 1
        fi
	elif [ $opt = "3" ]
	    then
	        exit 0
    else
            echo "Bad Character !!!!"
		    exit 1
	fi


done

}

show_menu
choice_scan(){
case $opt in

1) echo "Furtiv Scan :"
   echo "Give Target IP Address"
   read target
   perso_scan
;;
2) echo "TCP Scan :"
   echo "Give Target IP Address"
   read target
   perso_scan
;;
3) echo "UDP Scan :"
   echo "Give Target IP Address"
   read target
   perso_scan
;;
4) echo "Evasion Scan"
   echo "Give Target IP Address"
   read target
   perso_scan
;;
* | 5)
  echo "Exit"
  exit 0
;;
esac
}
choice_scan




